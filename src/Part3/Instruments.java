package Part3;

public abstract class Instruments {

    public abstract void play();

    public void music(){
        play();
        System.out.println("The music is playing!");
    }

}
