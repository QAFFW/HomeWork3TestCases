package Part1;

public interface Animal {

    public void checkStatus();
    public int getAge();
    public void run(int feet);
}