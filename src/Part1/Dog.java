package Part1;

public class Dog implements Animal {

    int age; //Using instance variables to model our Dog class after a real-life dog

    public Dog(int dogsAge) { //Constructor for the Dog class with parameters
        age = dogsAge;
    }
    public int getAge(){ //Used data type keywords (such as int, char, etc.) to specify that a method should return a value of that type.
        return age;
    }
    public void bark() {
        System.out.println("Woof!"); //bark method
    }

    public void run(int feet) { // Run method that accepts an int parameter called feet
        System.out.println("Your dog ran " + feet + " feet!"); //Calling the run method on spike will result in printing "Your dog ran 4 feet!" to the console.
    }

    public void checkStatus() { //The void keyword (which means "completely empty") indicates to the method that no value is returned after calling that method.
        System.out.println("Your dog is healthy and happy!");
    }

    public static void main(String[] args) { //main method
        Dog spike = new Dog(3); //New object created for the Dog class
        spike.bark(); //Calling a method on an spike object using the Dog class
        spike.run(4); //Calling the run method on the spike object and provide an int parameter of 4.

        int spikeAge = spike.getAge();
        System.out.println(spikeAge);

        spike.checkStatus();
    }

}