package Part1;

public class Cat implements Animal {

    int age;

    public Cat(int catsAge) {
        age = catsAge;
    }

    public void Meow() {
        System.out.println("Meow!");
    }

    public void run(int feet) {
        System.out.println("Your cat ran " + feet + " feet!");
    }

    public void checkStatus() {

        System.out.println("Your cat is healthy and happy!");

    }

    public int getAge(){
        return age;
    }

    public static void main(String[] args) {
        Cat flafy = new Cat(3);
        flafy.Meow();
        flafy.run(4);

        int flafyAge = flafy.getAge();
        System.out.println(flafyAge);

        flafy.checkStatus();
    }
}
