package Part2;

public abstract class Flying implements Vehicle{

    int speed;
    int capacity;

    public void move(){
        System.out.println("Your flying vehicle is flying!");
    }

    public int getSpeed(){
        return speed;
    }

}
